<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 8/28/2016
 * Time: 12:16 AM
 */



include_once "../../vendor/autoload.php";
use App\Bills\Bills;
$obj=new Bills();

if(isset($_SESSION['user']) && !empty($_SESSION['user'])){


    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8" />
        <title>Registration form Template | PrepBootstrap</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />

        <script type="text/javascript" src="../../js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <!--<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />-->

        <!-- <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
         <script type="text/javascript" src="js/bootstrap.min.js"></script>-->
    </head>
    <body>

    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Electicity Bills</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="adminIndex.php">Back to index</a></li>
                        <!--<li><a href="login.php">Login</a></li>-->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">
                                <?php
                                echo ucfirst($_SESSION['user']['username']);
                                ?>
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="../../Views/Bills/signout.php">Sign Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- Registration form - START -->
        <div class="col-md-12">
            <?php
            if(isset($_SESSION['massage'])){ ?>
                <div class="alert alert-success">
                    <strong><span>
                                    <?php
                                    echo $_SESSION['massage'];
                                    unset($_SESSION['massage']);
                                    ?>
                                </span></strong>
                </div>
            <?php }
            ?>
        </div>
        <div class="row">
            <form role="form" action="create2-process.php" method="POST">
                <div class="col-md-offset-3 col-md-6">
                    <!--<div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong></div>-->
                    <!-- <div class="form-group">
                         <label for="InputName">Full Name</label>
                         <div class="input-group">
                             <input type="text" class="form-control" name="FullName" id="InputName" placeholder="Enter Full Name" >
                             <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                         </div>
                     </div>-->
                    <div class="form-group">
                        <fieldset class="scheduler-border">
                            <fieldset>
                                <legend>Estimation of Electicity</legend>
                                <label for="">Present Unit Reading</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="pur" id="InputName" placeholder="Enter Present Unit Reading" >
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>
                                <label for="">Last Month Unit Reading</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="lmur" id="InputName" placeholder="Enter Last Month Unit Reading" >
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>

                                <label for="">Month</label>
                                <select class="form-control" id="sel1" name="month">
                                    <!--<option>select your blood group</option>-->
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                </select>
                                <label for="">Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="" name="date" placeholder="Enter Date" >
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>
                            </fieldset>
                    </div>
                    <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
                </div>


                <!-- <div class="form-group">
                     <input type="file" class="form-control" name="pp">
                 </div>-->
                <!--<div class="form-group">
                    <label for="InputName">Phone Number</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="phone" id="InputName" placeholder="Enter Phone Number" >
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>-->
                <!-- <div class="form-group">
                     <label for="Email">Enter Email</label>
                     <div class="input-group">
                         <input type="text" class="form-control" id="Email" name="email" placeholder="Enter Email" >
                         <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                     </div>
                 </div>-->
                <!-- <div class="form-group">
                     <fieldset class="scheduler-border">
                         <legend class="scheduler-border">Address</legend>
                         <label for="InputName">Address</label>
 
                         <input type="text" class="form-inline" name="add1" id="InputName" placeholder="Address Line 1" >
 
                         <label for="InputName">Address</label>
 
                         <input type="text" name="add2" id="InputName" placeholder="Address Line 2" >
 
 
                         <label for="InputName">City</label>
 
                         <input type="text"  name="add3" id="InputName" placeholder="City" >
 
                         <label for="InputName">Zip</label>
 
                         <input type="text"  name="add4" id="InputName" placeholder="Zip" >
                     </fieldset>
                 </div>-->
                <!-- <div class="form-group">
                    <label for="Gender">Gender</label><br>
                    <label class="radio-inline"><input type="radio" name="gender" value="male" <?php /*//if($data['gender']=='male'){echo "checked";}*/?>>Male</label>
                    <label class="radio-inline"><input type="radio" name="gender" value="female"<?php /*//if($data['gender']=='female'){echo "checked";}*/?>>Female</label
                </div>-->




            </form>
        </div>

    </div>

    <!-- Registration form - END -->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../js/bootstrap.min.js"></script>
    </body>
    </html>
<?php } ?>