<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 8/29/2016
 * Time: 12:51 AM
 */

include_once "../../vendor/autoload.php";
use App\Bills\Bills;
$obj=new Bills();
$edit=$obj->prepare($_GET)->show();
//echo "<pre>";
//print_r($edit);
if(isset($_SESSION['user']) && !empty($_SESSION['user'])){


    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8" />
        <title>Registration form Template | PrepBootstrap</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link href="../../css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />

        <script type="text/javascript" src="../../js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        
    </head>
    <body>

    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Trainer</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="adminIndex.php">Back to index</a></li>
                        <!--<li><a href="login.php">Login</a></li>-->
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">
                                <?php
                                echo ucfirst($_SESSION['user']['username']);
                                ?>
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="../../Views/Bills/signout.php">Sign Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- Registration form - START -->
        <div class="col-md-12">
            <?php
            if(isset($_SESSION['massage'])){ ?>
                <div class="alert alert-success">
                    <strong><span>
                                    <?php
                                    echo $_SESSION['massage'];
                                    unset($_SESSION['massage']);
                                    ?>
                                </span></strong>
                </div>
            <?php }
            ?>
        </div>
        <div class="row">
            <form role="form" action="update.php" method="POST">
                <div class="col-md-offset-3 col-md-6">
                    <div class="form-group">
                        <fieldset class="scheduler-border">
                            <fieldset>
                                <legend>Estimation of Masud Vai</legend>
                                <label for="">Amount</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="amount" id="InputName" value="<?php echo $edit['amount']?>" >
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>

                                <label for="">Month</label>
                                <select class="form-control" id="sel1" name="month">
                                    <!--<option>select your blood group</option>-->
                                    <option value="January" <?php if($edit['month']=='January'){echo "selected";}?>>January</option>
                                    <option value="February" <?php if($edit['month']=='February'){echo "selected";}?>>February</option>
                                    <option value="March" <?php if($edit['month']=='March'){echo "selected";}?>>March</option>
                                    <option value="April" <?php if($edit['month']=='April'){echo "selected";}?>>April</option>
                                    <option value="May" <?php if($edit['month']=='May'){echo "selected";}?>>May</option>
                                    <option value="June" <?php if($edit['month']=='June'){echo "selected";}?>>June</option>
                                    <option value="July" <?php if($edit['month']=='July'){echo "selected";}?>>July</option>
                                    <option value="August" <?php if($edit['month']=='August'){echo "selected";}?>>August</option>
                                    <option value="September" <?php if($edit['month']=='September'){echo "selected";}?>>September</option>
                                    <option value="October" <?php if($edit['month']=='October'){echo "selected";}?>>October</option>
                                    <option value="November" <?php if($edit['month']=='November'){echo "selected";}?>>November</option>
                                    <option value="December" <?php if($edit['month']=='December'){echo "selected";}?>>December</option>
                                </select>
                                <label for="">Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="" name="date" value="<?php echo $edit['date']?>" >
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>
                                <!--<div class="input-group">
                                    <input type="text" class="form-control" id="" name="total" value="<?php /*echo $edit['total_amount']*/?>" >
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>-->
                                <input type="hidden" name="id" value="<?php echo $edit['id']?>">
                            </fieldset>
                    </div>
                    <input type="submit" name="submit" id="submit" value="Update" class="btn btn-info pull-right">
                </div>
            </form>
        </div>
    </div>

    <!-- Registration form - END -->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../js/bootstrap.min.js"></script>
    </body>
    </html>
<?php } ?>