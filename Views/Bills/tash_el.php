<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 8/31/2016
 * Time: 11:42 PM
 */

include_once "../../vendor/autoload.php";
use App\Bills\Bills;
$obj=new Bills();
$eleticity_all=$obj->tash_el();
//echo "<pre>";
//echo "<pre>";
//print_r($masud_alldata);



if(isset($_SESSION['user']) && !empty($_SESSION['user'])){

//print_r($_SESSION['user']);

?>






<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
    <style>
        table{
            width: 400px;
            margin-bottom: 20px;
        }
        table td{
            padding:10px;
            text-align: center;
        }
        table th{
            padding:20px 30px;
            text-align: center;
            background: #00a8c6;
        }
    </style>
    <!-- Bootstrap -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="../../js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">DO IT</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <!-- <li><a href="Views/User/signup.php">Sign Up</a></li>-->
                    <li><a href="../../Views/Bills/create.php">ADD Masud Bills</a></li>
                    <li><a href="../../Views/Bills/create2.php">ADD Current Bills</a></li>
                    <li><a href="../../Views/Bills/create3.php">ADD House Rent Bills</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true"
                           aria-expanded="false">
                            <?php
                            echo ucfirst($_SESSION['user']['username']);
                            ?>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="../../Views/Bills/signout.php">Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="alert alert-success">


        <h1>This is Admin area</h1>

    </div>
    <div class="col-md-12">
        <?php
        if(isset($_SESSION['massage'])){ ?>
            <div class="alert alert-success">
                <strong><span>
                                    <?php
                                    echo $_SESSION['massage'];
                                    unset($_SESSION['massage']);
                                    ?>
                                </span></strong>
            </div>
        <?php }
        ?>
    </div>
    <div class="text-center">
        <table class="eletable" border="2" align="center">
            <th colspan="11">Estimation of Electicity</th>
            <tr>
                <th>ID</th>
                <th>Present Unit Reading</th>
                <th>Last Month Unit Reading</th>
                <th>Month</th>
                <th>Total Unit This month</th>
                <th>Total Taka For Unit This month</th>
                <th>Date</th>
                <th>Created Date and Time</th>
                <th colspan="3">Action</th>
            </tr>
            <?php
            $sl=1;
            if(isset($eleticity_all) && !empty($eleticity_all)){
                foreach ($eleticity_all as $eleticity_all_single){

                    ?>
                    <tr>
                        <td><?php echo $sl++; ?></td>
                        <td><?php echo $eleticity_all_single ['pre_unit']?></td>
                        <td><?php echo $eleticity_all_single ['past_unit']?></td>
                        <td><?php echo $eleticity_all_single ['month']?></td>
                        <td><?php echo $eleticity_all_single ['net_unit']?></td>
                        <td><?php echo $eleticity_all_single ['t_rate'] ?></td>
                        <td><?php echo $eleticity_all_single ['date'] ?></td>
                        <td><?php echo $eleticity_all_single ['created'] ?></td>
                        <td><a href="elview.php?id=<?php echo $eleticity_all_single ['id']?>">View</a></td>
                        <td><a href="edit.php?id=<?php echo $eleticity_all_single ['id']?>">Edit</a></td>
                        <td><a href="rstore.php?id=<?php echo $eleticity_all_single ['id']?>">Restore</a></td>

                    </tr>
                <?php }
            }else{
                ?>
                <tr>
                    <td colspan="8"><?php echo "No Avaiable Data"?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../js/bootstrap.min.js"></script>
</body>
    </html>
<?php }else{
    $_SESSION['Message'] = "Login for continue";
    header('location:login.php');
}
