<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 8/28/2016
 * Time: 4:03 PM
 */

include_once "../../vendor/autoload.php";
use App\Bills\Bills;
$obj=new Bills();
//print_r($_POST);

if($_SERVER['REQUEST_METHOD']=="POST"){
    if(!empty($_POST['amount'])){
        if(strlen($_POST['amount'])>=4){
            if(!empty($_POST['date'])){
                $obj->prepare($_POST)->store();
            }else{
                $_SESSION['massage']="Date can't be empty";
                header('location:create.php');
            }
        }else{
            $_SESSION['massage']="Amount must be 4 char like 9000";
            header('location:create.php');
        }
    }else{
        $_SESSION['massage']="Amount can't be empty";
        header('location:create.php');
    }
}else{
    $_SESSION['massage']="Opps Something Going Wrong!";
    header('location:create.php');
}