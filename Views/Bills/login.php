<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 8/27/2016
 * Time: 5:50 PM
 */
include_once "../../vendor/autoload.php";
use App\Bills\Bills;
$obj=new Bills(); 
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Registration form Template | PrepBootstrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css" />
    <!--<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />-->

    <!-- <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
     <script type="text/javascript" src="js/bootstrap.min.js"></script>-->
</head>
<body>

<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">DO IT</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <!--<li><a href="login.php">Login</a></li>-->
                    <!--<li><a href="signup.php">Sign Up</a></li>-->
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <!-- Registration form - START -->

    <div class="row">
        <form role="form" action="login-process.php" method="POST">
            <div class="col-lg-6">
                <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong></div>
                <div class="form-group">
                    <label for="InputName">User Name</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="UserName" id="InputName" placeholder="Enter User Name" >
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Password">Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control" id="Password" name="password" placeholder="Enter Password" >
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
            </div>
        </form>
        <div class="col-md-12">
            <?php
            if(isset($_SESSION['massage'])){ ?>
                <div class="alert alert-success">
                    <strong><span>
                                    <?php
                                    echo $_SESSION['massage'];
                                    unset($_SESSION['massage']);
                                    ?>
                                </span></strong>
                </div>
            <?php }
            ?>
        </div>
    </div>

    <!-- Registration form - END -->

</div>

</body>
</html>
