<?php

/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 8/27/2016
 * Time: 5:23 PM
 */
namespace App\Bills;
use PDO;


class Bills
{
    //for connection
    public $connection="";
    public $dbuser="root";
    public $dbpass="";

    //for login
    public $username="";
    public $password="";
    
    //for universal 
    public $month="";
    public $date="";
    public $id="";
    
    //for store in masud
    public $amount="";
    
    public $total_amot="";
    public $alldata_masud="";
    

    //for store in Electicity
    public $present_unit="";
    public $past_unit="";
    public $m_total="";
    public $rate_total="";
    public $all_data_elec="";
    public $alldata_el="";

    public function __construct()
    {
        session_start();
        $this->connection=new PDO('mysql:host=localhost;dbname=bill', $this->dbuser, $this->dbpass);
    }
    public function prepare($data=""){
        /*****************************************
         * for login
        *****************************************/
        if(!empty($data['UserName'])){
            $this->username=$data['UserName'];
        }
        if(!empty($data['password'])){
            $this->password=$data['password'];
        }
        /*****************************************
         * for store masud
         *****************************************/
        if(!empty($data['amount'])){
            $this->amount=$data['amount'];

        }
        /*****************************************
         * for universal
         *****************************************/
        if(!empty($data['month'])){
            $this->month=$data['month'];
        }
        if(!empty($data['date'])){
            $this->date=$data['date'];
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        /*****************************************
         * for store Electicity
         *****************************************/
        if(!empty($data['pur'])){
             $this->present_unit=$data['pur'];
        }
        if(!empty($data['lmur'])){
             $this->past_unit=$data['lmur'];
        }
        if(isset($data['pur'])&& $data['lmur']){
            $this->m_total=$net_unit=$data['pur']-$data['lmur'];
            if($this->m_total=$net_unit=$data['pur']-$data['lmur']){
                 $this->rate_total=$net_unit*5.5;
            }
        }

//        echo "this data output from class"."<br>";
//        echo "<pre>";
//        print_r($data);
        
        return $this;
    }

    public function login(){
        $username="'$this->username'";
        $password="'$this->password'";
        $sql="SELECT * FROM admin WHERE username= $username && password=$password";
        $stmt=$this->connection->prepare($sql);
        $stmt->execute();
        $user=$stmt->fetch(PDO::FETCH_ASSOC);
//        echo "<pre>";
//        print_r($user);
//die();
        if(isset($user) && !empty($user)){
            if($user['is_active']==0){
                $_SESSION['massage']= "<h3>Your account not verified yet. Check your email and verify</h3>";
                if($_SESSION['user']['is_admin']==1){
                    header('location:index.php');
                }
            }else{
                $_SESSION['user']=$user;
                header('location:adminIndex.php');
            }
        }else{
            $_SESSION['massage']="Invalide user name or password! ";
            header('location:login.php');
        }
    }

    /****************************************************
     * for masud vai
     ****************************************************/
    public function store(){

        //echo "store function";
        try{

            $tot="SELECT SUM(amount) FROM masud";
            $st=$this->connection->prepare($tot);
            $st->execute();
            //$ld=serialize($d);
            while ($d=$st->fetch(PDO::FETCH_ASSOC)){
                $this->total_amot[]=$d['SUM(amount)'];
                //print_r($this->total_amot['0']) ;die();
            }

            $sql="INSERT INTO masud (id,amount,month,date,date_database,is_deleted,total_amount) VALUES (:id,:amout,:mont,:dat,:datd,:isd,:toam)";
            $stmt=$this->connection->prepare($sql);
            $stmt->execute(array(
                ':id'=>null,
                ':amout'=>$this->amount,
                ':mont'=>$this->month,
                ':dat'=>$this->date,
                ':datd'=>date('Y:m:d, h:i:s' ),
                ':isd'=>'0',
                ':toam'=>$this->total_amot['0']+$this->amount,
            ));


            $_SESSION['massage']="Sucessfully Added";
            header('location:create.php');

        }catch (PDOException $e){
            echo "Error..." . $e->getMessage();
        }
        
    }


    public function index(){
        try{
            $sql="SELECT * FROM masud WHERE is_deleted=0";
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            while ($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                $this->alldata_masud[]=$row;
            }
           return $this->alldata_masud;
            //echo "<pre>";
            //print_r($row);
        }catch (PDOException $e){
            echo "Error.." . $e->getMessage();
        }
    }
    public function show(){
        //echo "single view";
        try{
            $sql="SELECT * FROM masud WHERE id=" . "'".$this->id."'";
            //echo $sql;die();
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            $row=$stmt->fetch(PDO::FETCH_ASSOC);
            return $row;
            //print_r($row);
        }catch (PDOException $e){
            echo "Error..." . $e->getMessage();
        }
    }
    public function softdeleted(){
        try{
            $sql="UPDATE masud SET is_deleted = '1' WHERE id =" . "'".$this->id."'";
           //echo $sql;die();
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            $_SESSION['massage']="Successfully Deleted";
            header('location:adminIndex.php');
        }catch (PDOException $e){
            echo "Error.." . $e->getMessage();
        }
    }
    public function tash(){
        try{
            $sql="SELECT * FROM masud WHERE is_deleted=1";
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            while ($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                $this->alldata_masud[]=$row;
            }
            return $this->alldata_masud;
        }catch (PDOException $e){
            echo "Error..." . $e->getMessage();
        }
    }
    public function restore(){
        try{
            $sql="UPDATE masud SET is_deleted = '0' WHERE masud.id =" . "'".$this->id."'";
            //echo $sql;
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            $_SESSION['massage']="Successfully Restored";
            header('location:adminIndex.php');
        }catch (PDOException $e){
            echo "Error.." . $e->getMessage();
        }
    }
    public function uadate(){
        try{
            $sql="UPDATE masud SET amount = :amout, month = :mont, date = :dat, date_database = :datd WHERE masud.id = " . "'".$this->id."'";
            //echo "$sql";
            $stmt=$this->connection->prepare($sql);
            $stmt->execute(array(
                ':amout'=>$this->amount,
                ':mont'=>$this->month,
                ':dat'=>$this->date,
                ':datd'=>date('Y-m-d h:i:s' ),
                //':toam'=>$this->total_amot - $this->amount,total_amount = :toam
            ));

            $_SESSION['massage']="Sucessfully Updated";
            header('location:adminIndex.php');
        }catch (PDOException $e){
            echo "Error.." . $e->getMessage();
        }
    }


    /****************************************************
     * for electicity
     ****************************************************/


    public function electicityStore(){
        try{
           $sql="INSERT INTO `electicity` (`id`, `pre_unit`, `past_unit`, `month`, `net_unit`, `t_rate`, `date` , `is_deleted`, `created`, `updated`, `deleted`) VALUES (NULL, '$this->present_unit','$this->past_unit','$this->month','$this->m_total','$this->rate_total','$this->date','0','".date('Y-m-d h:m:s',time())."','','')";
           $stmt=$this->connection->prepare($sql);
           $stmt->execute();
            /*echo $sql;
            die();*/

            /*$sql="INSERT INTO electicity (id,pre_unit,past_unit,net_unit,t_rate,is_deleted,created,updated,deleted) VALUES (:id,:pru,:pau,:neu,:tt,:isd,:crt,,:upd,:deld)";
            $stmt=$this->connection->prepare($sql);
            $stmt->execute(array(
                ':id'=>null,
                ':pru'=>$this->present_unit,
                ':pau'=>$this->past_unit,
                ':neu'=>$this->m_total,
                ':tt'=>$this->rate_total,
                ':isd'=>'0',
                ':crt'=>date('Y:m:d, h:i:s'),
                ':upd'=>'',
                ':deld'=>'',
            ));*/

            $_SESSION['massage']="Sucessfully Added";
            header('location:create2.php');

        }catch (PDOException $e){
            echo "Error.." . $e->getMessage();
        }

    }
    public function eleIndex(){
        try{
            $sql="SELECT * FROM electicity WHERE is_deleted=0";
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            //$row=$stmt->execute(PDO::FETCH_ASSOC);
            while ($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                $this->all_data_elec[]=$row;
            }
            return $this->all_data_elec;
            //$_SESSION['massage']="Sucessfully Added";
            //header('location:adminIndex.php');
        }catch (PDOException $e){
            echo "Error.." . $e->getMessage();
        }
    }
    
    public function singleShow(){
        try{
            $sql="SELECT * FROM electicity WHERE  id=" . "'".$this->id."'";
            //echo $sql;
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            $row=$stmt->fetch(PDO::FETCH_ASSOC);
            return $row;
        }catch (PDOException $e){
            echo "Error.." . $e->getMessage();
        }
    }
    public function softdeleted_el(){
        try{
            $sql="UPDATE electicity SET is_deleted = '1' WHERE id =" . "'".$this->id."'";
            //echo $sql;die();
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            $_SESSION['massage']="Successfully Deleted";
            header('location:adminIndex.php');
        }catch (PDOException $e){
            echo "Error.." . $e->getMessage();
        }
    }

    public function tash_el(){
        try{
            $sql="SELECT * FROM electicity WHERE is_deleted=1";
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            while ($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                $this->alldata_el[]=$row;
            }
            return $this->alldata_el;
        }catch (PDOException $e){
            echo "Error..." . $e->getMessage();
        }
    }
    public function restore_el(){
        try{
            $sql="UPDATE electicity SET is_deleted = '0' WHERE id =" . "'".$this->id."'";
            //echo $sql;
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            $_SESSION['massage']="Successfully Restored";
            header('location:adminIndex.php');
        }catch (PDOException $e){
            echo "Error.." . $e->getMessage();
        }
    }
    public function el_Update(){
        try{
            $sql="UPDATE `electicity` SET `pre_unit` = '$this->present_unit', `past_unit` = '$this->past_unit', `month` = '$this->month', `net_unit` = '$this->m_total', `t_rate` = '$this->rate_total', `date` = '$this->date' , `updated` = '".date('Y-m-d h:m:s',time())."'  WHERE `electicity`.`id` =" . "'". $this->id ."'";
           // echo $sql;die();
            $stmt=$this->connection->prepare($sql);
            $stmt->execute();
            /*$sql="UPDATE electicity SET pre_unit = :preu, past_unit = :pastu, month = :mont, net_unit = :netr, t_rate =: trt, date = :dat, updated = :datd WHERE electicity.id =" . "'".$this->id."'";
            //echo "$sql";
            $stmt=$this->connection->prepare($sql);
            $stmt->execute(array(
                ':preu'=>$this->present_unit,
                ':pastu'=>$this->past_unit,
                ':mont'=>$this->month,
                ':netr'=>$this->m_total,
                ':trt'=>$this->rate_total,
                ':dat'=>$this->date,
                ':datd'=>date('Y-m-d h:i:s',time()),
                //':toam'=>$this->total_amot - $this->amount,total_amount = :toam
            ));*/

            $_SESSION['massage']="Sucessfully Updated";
            header('location:adminIndex.php');
        }catch (PDOException $e){
            echo "Error.." . $e->getMessage();
        }
    }
    

}